import 'package:flutter/material.dart';

class MainView extends StatelessWidget {
  const MainView({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(16.0),
      child: Wrap(
        children: [
          ViewItem(icon: Icons.desktop_windows_outlined, text: 'Desktop'),
          ViewItem(icon: Icons.download_outlined, text: 'Downloads'),
          ViewItem(icon: Icons.edit_note_outlined, text: 'Documents'),
          ViewItem(icon: Icons.image_outlined, text: 'Images'),
          ViewItem(icon: Icons.music_note_outlined, text: 'Music'),
          ViewItem(icon: Icons.video_collection_outlined, text: 'Videos'),
        ],
      ),
    );
  }
}

class ViewItem extends StatelessWidget {
  final IconData icon;
  final String text;

  const ViewItem({
    required this.icon,
    required this.text,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Icon(icon, size: 45),
          Text(text),
        ],
      ),
    );
  }
}
