// A custom input chip widget
//
// Allows any length of text for the label and shows the entire text
// in the chip. The default input chip truncates the text and shows
// ellipsis.

// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class CustomInputChip extends StatelessWidget {
  final IconData icon;
  final String label;
  final List<Widget> menuChildren;
  final String text;

  CustomInputChip({
    super.key,
    required this.icon,
    required this.label,
    this.menuChildren = const [],
    required this.text,
  });

  final _menuController = MenuController();

  @override
  Widget build(BuildContext context) {
    return MenuAnchor(
      controller: _menuController,
      menuChildren: menuChildren,
      child: InputChip(
        label: Text(text),
        avatar: Icon(
          icon,
          color: Colors.white.withOpacity(0.8),
        ),
        onDeleted: () {},
        onPressed: menuChildren.isEmpty
            ? null
            : () {
                if (_menuController.isOpen) {
                  _menuController.close();
                } else {
                  _menuController.open();
                }
              },
        padding: EdgeInsets.all(4),
        visualDensity: VisualDensity(horizontal: -4, vertical: -4),
      ),
    );
  }
}
