enum DateFilter {
  any('Any Date'),
  today('Today'),
  yesterday('Yesterday'),
  thisWeek('This Week'),
  thisMonth('This Month'),
  thisYear('This Year');

  final String name;

  const DateFilter(this.name);
}
