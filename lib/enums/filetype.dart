enum Filetype {
  any('Any'),
  folders('Folders'),
  images('Images'),
  videos('Videos'),
  audio('Audio Files'),
  documents('Documents');

  final String name;

  const Filetype(this.name);
}
