import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit/cubit.dart';

class LocationBar extends StatelessWidget {
  const LocationBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        colorScheme: Theme.of(context).colorScheme.copyWith(
              primary: Colors.white.withOpacity(0.8),
            ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconButton(
            icon: const Icon(Icons.keyboard_arrow_left),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.keyboard_arrow_right),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.grid_view),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.view_comfortable_outlined),
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.account_tree_outlined),
            onPressed: () {},
          ),
          Expanded(
            child: Card(
              child: IntrinsicHeight(
                child: Row(
                  children: [
                    const SizedBox(width: 10),
                    TextButton(
                      onPressed: () {},
                      child: const Text('/  ⌄'),
                    ),
                    VerticalDivider(
                      color: Colors.grey.withOpacity(0.5),
                      thickness: 0.7,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: const Text('home'),
                    ),
                  ],
                ),
              ),
            ),
          ),
          TextButton.icon(
            onPressed: () {},
            icon: const Icon(Icons.vertical_split_outlined),
            label: const Text('Split'),
          ),
          BlocBuilder<MainCubit, MainState>(
            builder: (context, state) {
              return Card(
                color: state.showSearchBar ? null : Colors.transparent,
                elevation: 0,
                child: IconButton(
                  icon: const Icon(Icons.search),
                  isSelected: state.showSearchBar,
                  onPressed: () {
                    context.read<MainCubit>().toggleSearchBar();
                  },
                ),
              );
            },
          ),
          IconButton(
            icon: const Icon(Icons.menu),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
