import 'package:flutter/material.dart';

class PlacesColumn extends StatelessWidget {
  const PlacesColumn({super.key});

  @override
  Widget build(BuildContext context) {
    const header = Text(
      '    Places',
      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w200),
    );

    return SizedBox(
      width: 200,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          header,
          Transform.scale(
            scale: 0.9,
            alignment: Alignment.centerLeft,
            child: const ListTile(
              leading: Icon(Icons.home_outlined),
              title: Text('Home'),
            ),
          ),
          Transform.scale(
            scale: 0.9,
            alignment: Alignment.centerLeft,
            child: const ListTile(
              leading: Icon(Icons.desktop_windows_outlined),
              title: Text('Desktop'),
            ),
          ),
          Transform.scale(
            scale: 0.9,
            alignment: Alignment.centerLeft,
            child: const ListTile(
              leading: Icon(Icons.edit_note_outlined),
              title: Text('Documents'),
            ),
          ),
          Transform.scale(
            scale: 0.9,
            alignment: Alignment.centerLeft,
            child: const ListTile(
              leading: Icon(Icons.download_outlined),
              title: Text('Downloads'),
            ),
          ),
          Transform.scale(
            scale: 0.9,
            alignment: Alignment.centerLeft,
            child: const ListTile(
              leading: Icon(Icons.music_note_outlined),
              title: Text('Trash'),
            ),
          ),
        ],
      ),
    );
  }
}
