part of 'main_cubit.dart';

class MainState extends Equatable {
  final Filetype filetype;
  final bool showSearchBar;
  final bool showSearchOptions;

  const MainState({
    this.filetype = Filetype.images,
    this.showSearchBar = true,
    this.showSearchOptions = false,
  });

  @override
  List<Object> get props => [
        filetype,
        showSearchBar,
        showSearchOptions,
      ];

  MainState copyWith({
    Filetype? filetype,
    bool? showSearchBar,
    bool? showSearchOptions,
  }) {
    return MainState(
      filetype: filetype ?? this.filetype,
      showSearchBar: showSearchBar ?? this.showSearchBar,
      showSearchOptions: showSearchOptions ?? this.showSearchOptions,
    );
  }
}
