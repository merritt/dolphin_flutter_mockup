// ignore_for_file: prefer_const_constructors

import 'package:dolphin_flutter_mockup/enums/enums.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'main_state.dart';

class MainCubit extends Cubit<MainState> {
  MainCubit() : super(MainState());

  void setFiletype(Filetype filetype) {
    emit(state.copyWith(filetype: filetype));
  }

  void toggleSearchBar() {
    emit(state.copyWith(showSearchBar: !state.showSearchBar));
  }

  void toggleSearchOptions() {
    emit(state.copyWith(showSearchOptions: !state.showSearchOptions));
  }
}
