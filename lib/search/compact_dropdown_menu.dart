// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';
import '../enums/enums.dart';

class CompactDropdownMenu extends StatelessWidget {
  final List<DropdownMenuEntry<String>> dropdownMenuEntries;
  final bool enabled;
  final Color? fillColor;
  final String initialSelection;
  final Icon? leadingIcon;
  final Widget trailingIcon;

  const CompactDropdownMenu({
    required this.dropdownMenuEntries,
    this.enabled = true,
    this.fillColor,
    required this.initialSelection,
    this.leadingIcon,
    this.trailingIcon = const Icon(Icons.arrow_drop_down),
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    const dropdownWidth = 130.0;

    return DropdownMenu<String>(
      enabled: enabled,
      dropdownMenuEntries: dropdownMenuEntries,
      initialSelection: initialSelection,
      inputDecorationTheme: InputDecorationTheme(
        filled: true,
        fillColor: fillColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        constraints: BoxConstraints.tight(const Size(dropdownWidth, 30)),
        contentPadding: const EdgeInsets.symmetric(horizontal: 20),
        isDense: true,
      ),
      leadingIcon: leadingIcon,
      onSelected: (value) {
        context.read<MainCubit>().setFiletype(Filetype.values.firstWhere(
              (e) => e.name == value,
            ));
      },
      textStyle: Theme.of(context).textTheme.bodySmall,
      // Adjust the trailingIcon position back to the middle-right.
      trailingIcon: Transform.translate(
        offset: const Offset(3, -5),
        child: trailingIcon,
      ),
      width: dropdownWidth,
    );
  }
}
