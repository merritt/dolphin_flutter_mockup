// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:dolphin_flutter_mockup/enums/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';
import 'search.dart';

class PopoverSearchOptions extends StatelessWidget {
  const PopoverSearchOptions({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final subtitleTextStyle = TextStyle(
      fontSize: 12,
    );

    return ListView(
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        bottom: 16.0,
      ),
      children: [
        AdvancedSearchArea(),
        SizedBox(height: 10),
        SwitchListTile(
          value: false,
          onChanged: (value) {},
          title: Text('Case Sensitive'),
          subtitle: Text(
            'Match the exact case of the search term',
            style: subtitleTextStyle,
          ),
        ),
        SwitchListTile(
          value: false,
          onChanged: (value) {},
          title: Text('Whole Words'),
          subtitle: Text(
            'Match whole words only',
            style: subtitleTextStyle,
          ),
        ),
        SwitchListTile(
          value: true,
          onChanged: (value) {},
          title: Text('RegEx'),
          subtitle: Text(
            'Search with regular expressions',
            style: subtitleTextStyle,
          ),
        ),
        SwitchListTile(
          value: false,
          onChanged: (value) {},
          title: Text('Match Diacritics'),
          subtitle: Text(
            'Exactly match special characters in search term',
            style: subtitleTextStyle,
          ),
        ),
      ],
    );
  }
}

class AdvancedSearchArea extends StatelessWidget {
  const AdvancedSearchArea({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<MainCubit, MainState>(
          builder: (context, state) {
            return Wrap(
              spacing: 15,
              runSpacing: 15,
              alignment: WrapAlignment.center,
              children: [
                SizedBox(width: double.infinity),
                LabelledSearchFilter(
                  label: 'Search in:',
                  control: SegmentedButton<String>(
                    multiSelectionEnabled: true,
                    selected: <String>{'File Name', 'Contents'},
                    onSelectionChanged: (value) {},
                    segments: [
                      ButtonSegment(
                        value: 'File Name',
                        label: Text('File Name'),
                      ),
                      ButtonSegment(
                        value: 'Contents',
                        label: Text('Contents'),
                      ),
                    ],
                  ),
                ),
                LabelledSearchFilter(
                  label: 'File Type:',
                  control: CompactDropdownMenu(
                    leadingIcon: Icon(Icons.image_outlined),
                    dropdownMenuEntries: Filetype.values
                        .map(
                          (e) => DropdownMenuEntry(
                            label: e.name,
                            value: e.name,
                          ),
                        )
                        .toList(),
                    initialSelection: state.filetype.name,
                  ),
                ),
                LabelledSearchFilter(
                  label: 'Date:',
                  control: CompactDropdownMenu(
                    leadingIcon: Icon(Icons.calendar_today_outlined),
                    dropdownMenuEntries: [
                      DropdownMenuEntry(
                        label: 'Any',
                        value: 'Any',
                      ),
                      DropdownMenuEntry(
                        label: 'Today',
                        value: 'Today',
                      ),
                    ],
                    initialSelection: 'Today',
                  ),
                ),
                LabelledSearchFilter(
                  label: 'Rating:',
                  control: CompactDropdownMenu(
                    leadingIcon: Icon(Icons.star_border_outlined),
                    dropdownMenuEntries: [
                      DropdownMenuEntry(
                        label: 'Any',
                        value: 'Any',
                      ),
                      DropdownMenuEntry(
                        label: '1 or more',
                        value: '1 or more',
                      ),
                    ],
                    initialSelection: 'Any',
                  ),
                ),
                LabelledSearchFilter(
                  label: 'Tags:',
                  control: CompactDropdownMenu(
                    leadingIcon: Icon(Icons.label_outline),
                    dropdownMenuEntries: [
                      DropdownMenuEntry(
                        label: 'Any',
                        value: 'Any',
                      ),
                      DropdownMenuEntry(
                        label: '1 or more',
                        value: '1 or more',
                      ),
                    ],
                    initialSelection: 'Any',
                  ),
                ),
                LabelledSearchFilter(
                  label: 'Size:',
                  control: CompactDropdownMenu(
                    leadingIcon: Icon(Icons.sort),
                    dropdownMenuEntries: [
                      DropdownMenuEntry(
                        label: 'Any',
                        value: 'Any',
                      ),
                      DropdownMenuEntry(
                        label: 'Images',
                        value: 'Images',
                      ),
                    ],
                    initialSelection: 'Any',
                  ),
                ),
              ],
            );
          },
        ),
      ],
    );
  }
}
