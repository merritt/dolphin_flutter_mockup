import 'package:flutter/material.dart';

class LabelledSearchFilter extends StatelessWidget {
  final String label;
  final Widget control;

  const LabelledSearchFilter({
    required this.label,
    required this.control,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(' $label'),
        const SizedBox(height: 4),
        control,
      ],
    );
  }
}
