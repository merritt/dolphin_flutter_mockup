// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:dolphin_flutter_mockup/enums/date.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:popover/popover.dart';

import '../cubit/cubit.dart';
import '../enums/enums.dart';
import 'search.dart';

const chipDarkBackgroundColor = Color.fromRGBO(50, 50, 50, 1);

class SearchBar extends StatefulWidget {
  const SearchBar({
    super.key,
  });

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  void initState() {
    super.initState();

    children.add(
      Stack(
        children: [
          CompactDropdownMenu(
            dropdownMenuEntries: [
              DropdownMenuEntry(
                label: 'RegEx',
                value: 'RegEx',
              ),
            ],
            enabled: false,
            initialSelection: 'RegEx',
            leadingIcon: Icon(Icons.code_outlined),
            fillColor: chipDarkBackgroundColor,
            trailingIcon: const SizedBox(),
          ),
          CustomTagCloseButton(
            onPressed: () {
              setState(() {
                children.removeLast();
              });
            },
          ),
        ],
      ),
    );
  }

  final _controller = TextEditingController();
  final _searchFieldFocusNode = FocusNode();

  bool searchEverywhere = false;
  bool searchStarted = true;
  bool searchFinished = false;

  void checkSearchState(BuildContext context) {
    if (_searchFieldFocusNode.hasFocus ||
        _controller.text.isNotEmpty ||
        context.read<MainCubit>().state.showSearchOptions) {
      setState(() {
        searchStarted = true;
      });
    } else {
      setState(() {
        searchStarted = false;
      });
    }

    if (_controller.text.isEmpty) {
      setState(() {
        searchFinished = false;
      });
    } else {
      setState(() {
        searchFinished = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    _controller.addListener(() {
      checkSearchState(context);
    });

    _searchFieldFocusNode.addListener(() {
      checkSearchState(context);
    });

    return BlocBuilder<MainCubit, MainState>(
      builder: (context, state) {
        if (!state.showSearchBar) {
          return SizedBox();
        }

        return Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Card(
            child: Padding(
              padding: EdgeInsets.only(
                left: 16.0,
                right: 8.0,
                top: 4.0,
                bottom: (state.showSearchOptions || searchStarted) ? 12.0 : 4,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: _controller,
                          focusNode: _searchFieldFocusNode,
                          decoration: InputDecoration(
                            hintText: 'Search...',
                            border: searchStarted ? null : InputBorder.none,
                            suffixIcon: (searchFinished)
                                ? IconButton(
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      _controller.clear();
                                    },
                                  )
                                : null,
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Theme(
                        data: Theme.of(context).copyWith(
                          colorScheme: Theme.of(context).colorScheme.copyWith(
                                primary: Colors.white.withOpacity(0.8),
                              ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: Directionality(
                            textDirection: TextDirection.rtl,
                            child: Builder(
                              builder: (context) {
                                return OutlinedButton.icon(
                                  onPressed: () async {
                                    await showPopover(
                                      context: context,
                                      direction: PopoverDirection.top,
                                      backgroundColor:
                                          Color.fromRGBO(73, 72, 75, 1),
                                      height: 600,
                                      width: 310,
                                      bodyBuilder: (context) {
                                        return PopoverSearchOptions();
                                      },
                                    );
                                  },
                                  icon: Icon(Icons.keyboard_arrow_down),
                                  label: Text('Filter'),
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  if (searchStarted)
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Wrap(
                        alignment: WrapAlignment.end,
                        runSpacing: 10,
                        spacing: 10,
                        children: [
                          SegmentedButton<String>(
                            selected: <String>{'Here'},
                            onSelectionChanged: (value) {},
                            segments: [
                              ButtonSegment(
                                value: 'Here',
                                label: Text('Here'),
                              ),
                              ButtonSegment(
                                value: 'Everywhere',
                                label: Text('Everywhere'),
                              ),
                            ],
                          ),
                          SegmentedButton<String>(
                            multiSelectionEnabled: true,
                            selected: <String>{'File Name', 'Contents'},
                            onSelectionChanged: (value) {},
                            segments: [
                              ButtonSegment(
                                value: 'File Name',
                                label: Text('File Name'),
                              ),
                              ButtonSegment(
                                value: 'Contents',
                                label: Text('Contents'),
                              ),
                            ],
                          ),
                          ...children,
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

final List<Widget> children = [
  BlocBuilder<MainCubit, MainState>(
    builder: (context, state) {
      return CompactDropdownMenu(
        leadingIcon: Icon(Icons.image_outlined),
        dropdownMenuEntries: Filetype.values
            .map(
              (e) => DropdownMenuEntry(
                label: e.name,
                value: e.name,
              ),
            )
            .toList(),
        fillColor: chipDarkBackgroundColor,
        initialSelection: state.filetype.name,
        trailingIcon: Icon(Icons.close),
      );
    },
  ),
  CompactDropdownMenu(
    leadingIcon: Icon(Icons.calendar_today_outlined),
    dropdownMenuEntries: DateFilter.values
        .map(
          (e) => DropdownMenuEntry(
            label: e.name,
            value: e.name,
          ),
        )
        .toList(),
    fillColor: chipDarkBackgroundColor,
    initialSelection: 'Today',
    trailingIcon: Icon(Icons.close),
  ),
  Stack(
    children: [
      CompactDropdownMenu(
        dropdownMenuEntries: [
          DropdownMenuEntry(
            label: 'RegEx',
            value: 'RegEx',
          ),
        ],
        enabled: false,
        initialSelection: 'RegEx',
        leadingIcon: Icon(Icons.code_outlined),
        fillColor: chipDarkBackgroundColor,
        trailingIcon: const SizedBox(),
      ),
      CustomTagCloseButton(
        onPressed: () {},
      ),
    ],
  ),
];

class ActiveTagsWrap extends StatefulWidget {
  ActiveTagsWrap({
    super.key,
  });

  @override
  State<ActiveTagsWrap> createState() => _ActiveTagsWrapState();
}

class _ActiveTagsWrapState extends State<ActiveTagsWrap> {
  @override
  void initState() {
    super.initState();

    children.add(
      Stack(
        children: [
          CompactDropdownMenu(
            dropdownMenuEntries: [
              DropdownMenuEntry(
                label: 'work',
                value: 'work',
              ),
            ],
            enabled: false,
            initialSelection: 'work',
            leadingIcon: Icon(Icons.label_outline),
            fillColor: chipDarkBackgroundColor,
            trailingIcon: const SizedBox(),
          ),
          CustomTagCloseButton(
            onPressed: () {
              setState(() {
                children.removeLast();
              });
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: BlocBuilder<MainCubit, MainState>(
        builder: (context, state) {
          return Wrap(
            spacing: 8,
            runSpacing: 10,
            alignment: WrapAlignment.end,
            children: children,
          );
        },
      ),
    );
  }
}

class CustomTagCloseButton extends StatelessWidget {
  final VoidCallback onPressed;

  CustomTagCloseButton({
    super.key,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 8,
      top: -1,
      child: Transform.scale(
        scale: 0.8,
        child: IconButton(
          visualDensity: VisualDensity.compact,
          padding: EdgeInsets.all(4),
          icon: Icon(Icons.close),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
