// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart' hide SearchBar;

import 'location_bar.dart';
import 'main_view.dart';
import 'places_column.dart';
import 'search/search_bar.dart';

class DolphinView extends StatelessWidget {
  const DolphinView({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LocationBar(),
        Expanded(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PlacesColumn(),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SearchBar(),
                          Expanded(child: MainView()),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
